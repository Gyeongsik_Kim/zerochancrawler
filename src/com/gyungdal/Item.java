package com.gyungdal;

/**
 * Created by GyungDal on 2016-10-21.
 */
public class Item {

    public final String url, path, name, desc;

    public Item(String url, String name, String path, String desc) {
        this.path = path;
        this.name = name;
        this.url = url;
        this.desc = desc;
    }
}
