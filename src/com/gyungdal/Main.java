package com.gyungdal;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Main {
    private static final String URLRL = "http://www.zerochan.net";
    public static void main(String[] args) {

        if(args.length == 0){
            System.err.println("사용 방법 : [프로그램] [주소] or [프로그램] [주소] -f [저장경로]");
            System.err.println("EXAMPLE : [프로그램] \"http://www.zerochan.net/Akabane+(Zebrasmise)\" or" +
                    "[프로그램] \"http://www.zerochan.net/Akabane+(Zebrasmise)\" -f \"D:\\\"");

            System.out.println("기본적으로 파일은 홈 디렉토리에 저장됩니다.");
            System.out.println("* 이상한 경로 넣어서 에러나면 알아서 하세요 ^^ *");
            System.out.println("멀티 스레드를 사용할거면 -m 맨 마지막 인자로 추가해서 돌리면 됩니다");
            System.out.println("멀티 스레드를 사용할거면...인터넷 빠른곳에서만 쓰세요...");
            return;
        }
        boolean multiThreading = false;

        String url = args[0];
        String savePath =  System.getProperty("user.home") + File.separator + "ImageSaver" +  File.separator + getTime() + File.separator;
        if(!url.contains("http://"))
            url = "http://" + url;

        for(int i = 0;i<args.length;i++){
            if(args[i].equals("-m"))
                multiThreading = true;
            if(args[i].equals("-f")) {
                if (i + 1 >= args.length)
                    System.err.println("주작하지마라 ^^ 끄지는 않을게 ^^");
                else {
                    if(args[i + 1].contains(File.separator))
                        savePath = args[i + 1] + "ImageSaver" + File.separator + getTime() + File.separator;
                    else
                        savePath = args[i + 1] + File.separator + "ImageSaver" + File.separator + getTime() + File.separator;
                }
            }
        }
        String next = "";
        try {
            if(url.contains("www.zerochan.net/")){
                System.out.println("URL CHECK!");
                Document doc = Jsoup.connect(url)
                        .get();
                int maxPage = getMaxPage(doc.select(".pagination").get(0).toString());
                int nowPage = 0;
                while(nowPage++ <= maxPage){
                    System.out.println(nowPage + "번 페이지 파싱 시작");
                    if(nowPage <= 100)
                        doc = Jsoup.connect(url + "?p=" + nowPage).get();
                    else
                        doc = Jsoup.connect(url + next).get();
                    Elements elements = doc.select("#thumbs2 > li");
                    System.out.println(elements.size() + "개의 이미지 발견");
                    for(int i = 0;i<elements.size();i++){
                        System.out.print(nowPage + "번 페이지, " + i + "번 이미지 주소 : ");
                        String imageDocUrl = elements.get(i).select("a").get(0).toString();
                        imageDocUrl = imageDocUrl.substring(imageDocUrl.indexOf("href=\"") + "href=\"".length());
                        imageDocUrl = imageDocUrl.substring(0, imageDocUrl.indexOf("\""));
                        imageDocUrl = URLRL + imageDocUrl;
                        System.out.println(imageDocUrl);
                        Document imageDoc = Jsoup.connect(imageDocUrl).get();
                        String imageUrl = imageDoc.select("#large").get(0).toString();
                        imageUrl = imageUrl.substring(imageUrl.indexOf("\"http://static.zerochan") + "\"".length());
                        imageUrl = imageUrl.substring(0, imageUrl.indexOf("\""));
                        System.out.println("이미지 상세 주소 : " + imageUrl);
                        final Item item = new Item(imageUrl,  i + "번 이미지." + getExt(imageUrl), savePath + nowPage + "페이지" + File.separator
                                ,nowPage + "페이지, " + i + "번 이미지 다운");
                        if(multiThreading) {
                            new Thread() {
                                @Override
                                public void run() {
                                    imgDown(item.url, item.path, item.name);
                                    System.out.println(item.desc);
                                }
                            }.start();
                        }else{
                            imgDown(item.url, item.path, item.name);
                            System.out.println(item.desc);
                        }
                    }
                    if(nowPage > 99) {
                        next = doc.toString();
                        if (next != null) {
                            next = next.substring(next.indexOf("?o="));
                            next = next.substring(0, next.indexOf("\""));
                            next = next.replaceAll("\"", "");
                            if(next.contains("&")){
                                next = next.substring(0, next.indexOf("&"));
                            }
                        }
                        System.out.println(next);
                    }
                }
                while(Thread.activeCount() > 1);
                System.out.println("다운로드 끝");
            }else{
                System.out.println("EXAMPLE URL TYPE : http://www.zerochan.net/Akabane+(Zebrasmise)");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getExt(String name){
        return name.substring(name.lastIndexOf('.') + 1, name.length());
    }

    private static int getMaxPage(String maxPage){
        maxPage = maxPage.substring(maxPage.indexOf("page 1 of ") + "page 1 of ".length());
        maxPage = maxPage.substring(0, maxPage.indexOf("<"));
        maxPage = maxPage.replaceAll(",", "");
        return Integer.valueOf(maxPage.trim());
    }
    private static String getName(){
        try{

        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
    private static String getTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        return dateFormat.format(cal.getTime());
    }
    private static void imgDown(String url, String path, String name){
        try{
            System.out.println("Save Path : " + path);
            System.out.println("Name : " + name);

            File file = new File(path);
            if(!file.exists()) {
                System.out.println("출력 폴더 생성");
                file.mkdirs();
            }
            file = new File(path + name);
            BufferedImage image = ImageIO.read(new URL(url));
            ImageIO.write(image, getExt(name), file);
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }
}
